package com.example.demo.exception;

import com.example.demo.model.Error;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * @author jakub
 * 18.04.2023
 */
@ControllerAdvice
public class ExceptionControllerAdvice {
    protected static final Log logger = LogFactory.getLog(ExceptionControllerAdvice.class);

    @ExceptionHandler(value = BadRequestException.class)
    public ResponseEntity<Error> badRequestExceptionHandler(BadRequestException ex, WebRequest request) {
        logger.warn(ex.getMessage());
        return createResponse(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UnauthorizedException.class)
    public ResponseEntity<Error> unauthorizedExceptionHandler(UnauthorizedException ex, WebRequest request) {
        logger.warn(ex.getMessage());
        return createResponse(HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<Error> entityNotFoundExceptionHandler(EntityNotFoundException ex, WebRequest request) {
        logger.warn(ex.getMessage());
        return createResponse(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = AlreadyExistsException.class)
    public ResponseEntity<Error> alreadyExistsExceptionHandler(AlreadyExistsException ex, WebRequest request) {
        logger.warn(ex.getMessage());
        return createResponse(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(value = {UnprocesableEntityException.class, MethodArgumentNotValidException.class})
    public ResponseEntity<Error> unprocesableExceptionHandler(Exception ex, WebRequest request) {
        logger.warn(ex.getMessage());
        return createResponse(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    private ResponseEntity<Error> createResponse(HttpStatus httpStatus) {
        return ResponseEntity.status(httpStatus).body(ErrorFactory.createError(httpStatus));
    }
}
