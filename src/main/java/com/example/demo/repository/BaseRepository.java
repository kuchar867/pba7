package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.transaction.Transactional;
import java.io.Serializable;

/**
 * @author jakub
 * 14.01.2022
 */
@NoRepositoryBean
@Transactional
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {
}
