package com.example.demo.utils;

import com.example.demo.model.ResponseHeader;
import org.joda.time.DateTime;

import java.util.UUID;

/**
 * @author jakub
 * 18.04.2023
 */
public class ResponseHeaderProvider {
    public static ResponseHeader provide() {
        ResponseHeader responseHeader = new ResponseHeader();
        responseHeader.setRequestId(UUID.randomUUID());
        responseHeader.setSendDate(DateTime.now());
        return responseHeader;
    }
}
