package com.example.demo.controller;

import com.example.demo.api.UsersApi;
import com.example.demo.entity.UserEntity;
import com.example.demo.model.*;
import com.example.demo.service.UserService;
import com.example.demo.utils.UserMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(path = "/api/v1")
public class UserController extends BaseController<UserEntity, String, User, UserService> implements UsersApi {

    private final UserMapper userMapper;

    public UserController(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public ResponseEntity<UserResponse> createUser(@RequestBody @Valid CreateRequest body) {
        return ResponseEntity.ok().body(userMapper.mapToResponse(service.save(userMapper.mapToEntity(body))));
    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID id) {
        service.delete(id.toString());
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers() {
        return ResponseEntity.ok().body(userMapper.mapToListResponse(service.findAll()));
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(UUID id) {
        return ResponseEntity.ok().body(userMapper.mapToResponse(service.findById(id.toString())));
    }

    @Override
    public ResponseEntity<UserResponse> updateUser(UUID id, @RequestBody @Valid UpdateRequest body) {
        return ResponseEntity.ok().body(userMapper.mapToResponse(service.update(id.toString(), userMapper.mapToEntity(body))));
    }
}
