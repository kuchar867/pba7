package com.example.demo.service;

import com.example.demo.entity.UserEntity;
import com.example.demo.repository.UserRepository;
import org.springframework.stereotype.Service;

/**
 * @author jakub
 * 15.04.2023
 */
@Service
public class UserService extends BaseService<UserEntity, String, UserRepository> {
}
